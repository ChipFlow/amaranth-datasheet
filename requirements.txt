sphinx
markupsafe==2.0.1
sphinxcontrib-mermaid
git+https://github.com/amaranth-lang/amaranth-boards
git+https://github.com/amaranth-lang/amaranth
sphinxcontrib-wavedrom
sphinx_rtd_theme
