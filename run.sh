#!/bin/bash
DIR=$1
setup="python $DIR/setup.py"
name=`$setup --name`
author=`$setup --author`
version=`$setup --version`
license=`$setup --license`





sphinx-build -b singlehtml -c . -D project="$name" -D copyright="$license"  -D author="$author" -D root_doc=$name
